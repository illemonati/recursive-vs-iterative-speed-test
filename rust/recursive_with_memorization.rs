

fn fib(n: u64) -> u64 {
    let mut save : Vec<u64> = vec![0; (n+1) as usize];
    save[1] = 1;
    save[2] = 1;
    _fib(n as usize, &mut save)
}



fn _fib(n: usize, save: &mut Vec<u64>) -> u64 {
    if n == 0 {
        return 0;
    }
    if save[n] == 0 {
        save[n] = _fib(n-1, save) + _fib(n-2, save);
    }
    return save[n];

}


fn main() {
    println!("{}", fib(45));
}
