

fn fib(n: usize) -> usize {
    _fib(n).0
}



fn _fib(n: usize) -> (usize, usize) {
    if n == 0 {
        return (0, 1);
    }
    let (fib_half_n, fib_half_n_plus_one) = _fib(n / 2);
    // println!("{}, {}, {}", n/2, fib_half_n, fib_half_n_plus_one);
    let fib_n = fib_half_n * (2 * fib_half_n_plus_one - fib_half_n);
    let fib_n_plus_one = fib_half_n * fib_half_n + fib_half_n_plus_one * fib_half_n_plus_one;
    if n & 1 == 1 {
        (fib_n_plus_one, fib_n_plus_one + fib_n)
    } else {
        (fib_n, fib_n_plus_one)
    }
}


fn main() {
    println!("{}", fib(45));
}
