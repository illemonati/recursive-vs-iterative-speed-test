
use std::collections::HashMap;

fn fib_fast_double_recursive_with_memorization(n: usize) -> usize {
    let mut dict = HashMap::new();
    dict.insert(0, 0);
    dict.insert(1, 1);
    dict.insert(2, 1);
    _fib_fast_double_recursive_with_memorization(n, &mut dict);
    return *dict.get(&n).unwrap();
}

fn _fib_fast_double_recursive_with_memorization(n: usize, dict: &mut HashMap<usize, usize>) {

    if (n == 0) || (n == 1) {
        return;
    }

    if (dict.contains_key(&n)) && (dict.contains_key(&(n+1))) {
        return;
    }

    let k = match dict.get(&(n/2)) {
        Some(k) => *k,
        None => {
            _fib_fast_double_recursive_with_memorization(n/2, dict);
            *dict.get(&(n/2)).unwrap()
        }
    };

    let kp1 = match dict.get(&((n/2)+1)) {
        Some(k) => *k,
        None => {
            _fib_fast_double_recursive_with_memorization((n/2)+1, dict);
            *dict.get(&((n/2)+1)).unwrap()
        }
    };


    let twok = k * ((2 * kp1) - k);
    let twokp1 = k * k + kp1 * kp1;
    if (n & 1) == 0 {
        dict.entry(n).or_insert(twok);
        dict.entry(n+1).or_insert(twokp1);
    } else {
        dict.entry(n-1).or_insert(twok);
        dict.entry(n).or_insert(twokp1);
        dict.entry(n+1).or_insert(twokp1+twok);
    }
}

fn main() {
    println!("{}", fib_fast_double_recursive_with_memorization(45));
}

