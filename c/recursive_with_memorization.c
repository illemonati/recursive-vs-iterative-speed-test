#include<inttypes.h>
#include<stdio.h>
#include<stdlib.h>



uint64_t _fib(uint64_t n, uint64_t * save) {
    if (n == 0) {
        return 0;
    }
    if (save[n] == 0) {
        save[n] = _fib(n-1, save) + _fib(n-2, save);
    }
    return save[n];
}



uint64_t fib(uint64_t n) {
    uint64_t * save = calloc(n+1, sizeof(uint64_t));
    save[1] = 1;
    save[2] = 1;
    uint64_t res = _fib(n, save);
    free(save);
    return res;
}


int main(int argc, char ** argv) {
    printf("%"PRIu64"\n", fib(45));
}




