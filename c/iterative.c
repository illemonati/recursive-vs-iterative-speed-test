#include<stdio.h>
#include<inttypes.h>


uint64_t fib(uint64_t n) {
    uint64_t a = 0;
    uint64_t b = 1;
    for (uint64_t i = 0; i < n; ++i) {
        uint64_t temp = a;
        a = b;
        b += temp;
    }
    return a;
}


int main(int argc, char ** argv) {
    printf("%" PRIu64 "\n", fib(45));
}
