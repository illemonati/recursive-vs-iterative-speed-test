#include <stdio.h>
#include <inttypes.h>

typedef struct {
    uint64_t fib_n;
    uint64_t fib_n_plus_one;
} fib_result_t;


fib_result_t _fib(uint64_t n) {
    if (n == 0) {
        return (fib_result_t){0, 1};
    }
    fib_result_t half_res = _fib(n / 2);
    uint64_t fib_n = half_res.fib_n * ( 2 * half_res.fib_n_plus_one - half_res.fib_n );
    uint64_t fib_n_plus_one = half_res.fib_n * half_res.fib_n + half_res.fib_n_plus_one * half_res.fib_n_plus_one;
    return (n & 1) ? (fib_result_t){fib_n_plus_one, fib_n_plus_one+fib_n} : (fib_result_t){fib_n, fib_n_plus_one};
}

uint64_t fib(uint64_t n) {
    return _fib(n).fib_n;
}

int main() {

    printf("%" PRIu64 "\n", fib(45));

    return 0;
}

